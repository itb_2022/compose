import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.compose.ui.window.rememberWindowState


val colors = listOf(Color.Red, Color.Blue, Color.Green, Color.Yellow, Color.Magenta, Color.Cyan)


@Preview
@Composable
fun Mastermind() {
    val comprovations = listOf(Color.White, Color.Gray, Color.Black, Color.White)
    val coloresFila2 = listOf(Color.Red, Color.Green, Color.Yellow, Color.Blue)
    val sizeCircle = 24
    var selectedColor by remember { mutableStateOf(Color.Red) }

    Column(
        modifier = Modifier
            .background(Color.White)
            //.border(10.dp, Color.Red, RectangleShape)
            //.height(500.dp)
            .padding(top = 50.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Row {
            Text(
                text = "M A S T E R M I N D",
                fontSize = 32.sp,
                fontFamily = FontFamily.Monospace,
                fontStyle = FontStyle.Normal,
                color = Color.Black
            )
            /*
            fontSize = 32.sp
            fontFamily = FontFamily(Font(R.font.your_custom_font, FontWeight.Bold))
            fontStyle = FontStyle.Italic
            color = Color.Black

             */

        }
        Row(
            modifier = Modifier
                .background(Color.White)
            //.height(500.dp)
            //.offset(y = (-100).dp)
            ,
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center

        ) {
            //SMALL 4 CIRCLES COLORS TO HELP USER ABOUT THE COLORS HE HAS CHOSE
            Column(
                modifier = Modifier
                    .weight(1f)
                 //   .fillMaxHeight()
                    .offset(x = (-46).dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.End
            ) {
                for (i in 0..5) {
                    Row() {
                        TwoRowsWithCircles(Color.Red)
                    }
                }
            }
            //4 CIRCLES COLORS'S USER
            Column(
                modifier = Modifier
                    .weight(1f)
                    .offset(x = (-24).dp, y = (-28).dp)

            ) {
                for (i in 0..5) {
                    Spacer(
                        modifier = Modifier
                            .height(34.dp)
                    )
                    Row() {
                        for (color in coloresFila2) {
                            Circulo(color, sizeCircle)
                            Spacer(
                                modifier = Modifier
                                    .width(4.dp)
                            )
                        }
                    }
                }
            }

        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Row(
                modifier = Modifier,
                   // .width(50.dp),
                //contentAlignment = Alignment.Center
            ){

                for (color in coloresFila2){
                    CirculoClickable(color,sizeCircle) {
                        selectedColor = color

                    }
                }
                Circulo(selectedColor,23)
            }
        }

    }
}

@Composable
fun TwoRowsWithCircles(color: Color) {
    var sizeCircle = 15
    Column {
        Row {
            Circulo(color, sizeCircle)
            Spacer(modifier = Modifier.width(4.dp))
            Circulo(color, sizeCircle)
        }
        Spacer(modifier = Modifier.height(4.dp))
        Row {
            Circulo(color, sizeCircle)
            Spacer(modifier = Modifier.width(4.dp))
            Circulo(color, sizeCircle)
        }
        Spacer(modifier = Modifier.height(24.dp))

    }
}

@Composable
fun Circulo(color: Color, size: Int) {
    Box(
        modifier = Modifier
            .size(size.dp)
            .background(color, shape = CircleShape)
    )

}


@Composable
fun CirculoClickable(color: Color, size: Int, onclick:(Unit)->Unit) {
    Box(
        modifier = Modifier
            .size(size.dp)
            .background(color, shape = CircleShape)
            .clickable {
                onclick
            }
    )

}

fun main() = application {
    Window(
        onCloseRequest = ::exitApplication,
        title = "Mastermind",
        state = rememberWindowState(width = 450.dp, height = 850.dp)
    ) {
        Mastermind()
    }
}
